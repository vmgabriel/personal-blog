---
title: Bienvenida a Blog Logos de Gabriel
description: Aqui hago explico mis planes a futuro con el blog y doy una fuerte bienvenida a mis lectores.
content_data: posts
tags: 
  - hexo
image: /images/post/bienvenida.png
altImage: Imagen de Bienvenida
date: 2020-04-17 08:59:10
categories:
  - blog personal
---
Welcome to [Hexo](https://hexo.io/)! This is your very first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).

## Quick Start

### Create a new post

``` bash
$ hexo new "My New Post"
$ cd ..
$ ls -la
$ echo "hello world"
```

More info: [Writing](https://hexo.io/docs/writing.html)

### Run server

``` bash
$ hexo server
```

More info: [Server](https://hexo.io/docs/server.html)

### Generate static files

``` bash
$ hexo generate
```

More info: [Generating](https://hexo.io/docs/generating.html)

### Deploy to remote sites

``` bash
$ hexo deploy
```

More info: [Deployment](https://hexo.io/docs/one-command-deployment.html)
