---
title: "Proyecto Logos de Hexo"
description: El proyecto logos es sin duda uno de los mas interesantes proyectos internos que tengo, mi objetivo es que sea la forma de dar abiertamente mis conocimientos a personas dispuestas en ponerlo en evidencia, sea generando un sano debate, como tambien apoyando los conceptos.
image: /images/projects/hexo.png
tags:
  - hexo
  - blog
  - javascript
  - pug
categories:
  - Diseño Web
  - Desarrollo
date: 2020-04-19 08:59:10
content_data: projects
percentComplete: 80
---

# Proyecto Logos Hexo

La idea general de este proyecto es generar un lugar comodo de generacion de conocimiento y aprendizaje, siendo el lector capaz de aprender algunos temas de interes que lo lleven a indagar acerca de como se puede hacer de otra manera o aportando a este u otro proyecto.

Como siempre considero que la primera cosa para uno entender el trabajo de los demas es generando una herramienta de comunicacion para que esta pueda ser facilmente interpretada y a traves del juicio de cada quien formar una conclusion un poco mas interesante y quizas aprender un poco mas.
