# Blog Personal
[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://spacemacs.org)

---

## Pagina
[Pagina Blog](https://vmgabriel.com "Pagina Personal")

## Descripcion
Proyecto personal especializado en generar una pagina blog personal acerca de temas de tecnologia con un tema personalizado y personal.

El proyecto esta basado en [Hexo](https://hexo.io "hexo link") que es un framework de trabajo para la construccion de blogs personales.

## Tecnologias
- Hexo
- Javascript
- Css 3
- FlexBox(Uno de los Retos Personales era hacerlo completamente con este)
- HTML
- Markdown
- Despliegue Continuo (CD)
- GITLAB

## Tema
El tema es personal, creado por mi persona, como desarrollador fue un reto hacerlo completamente yo, sin recurrrir a no mas de estas tecnologias, apenas se usa JQuery debido a ello no lo inclui dentro de la lista de tecnologias.

---

## Decisiones de Desarrollo
He estado involucrado en algunos cambios relacionados en la pagina, a simple vista no se nota porque no tiene nada que ver por el lado de front, sin embargo, muchos de esos cambios involucran en como actua sass dentro del stack de la plataforma.

## Creator
- Gabriel Vargas Monroy
